package com.example.studycase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

EditText garis1, garis2;
Button cek;
TextView hitung;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        garis1=(EditText) findViewById(R.id.garis1);
        garis2=(EditText) findViewById(R.id.garis2);
        cek = (Button) findViewById(R.id.cek);
        hitung = (TextView) findViewById(R.id.hitung);

        cek.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String isigaris1 = garis1.getText().toString();
                String isigaris2 = garis2.getText().toString();

                double garis1 = Double.parseDouble(isigaris1);
                double garis2 = Double.parseDouble(isigaris2);

                double hasil = luaslahan(garis1, garis2);
                String output = String.valueOf(hasil);
                hitung.setText(output.toString());

        }
    });
}

public double luaslahan(double garis1, double garis2) {
    return (garis1*garis2);
}
}