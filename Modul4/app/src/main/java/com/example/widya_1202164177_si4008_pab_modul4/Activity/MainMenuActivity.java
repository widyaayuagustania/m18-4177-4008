package com.example.dickson_1202160303_si4003_pab_modul4.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.dickson_1202160303_si4003_pab_modul4.Model.modelData;
import com.example.dickson_1202160303_si4003_pab_modul4.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;

import java.util.ArrayList;

public class MainMenuActivity extends AppCompatActivity {
    //instance for firebase
    private FirebaseStorage storage;
    private FirebaseAuth auth;
    private FirebaseFirestore db;
    private Button load;
    //instance for rv
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView recyclerView;
    private ArrayList<modelData> itemList;
    private AdapterData mAdapter;
    private static final String TAG = MainMenuActivity.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        itemList = new ArrayList<modelData>();
        auth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        //recycler
        RecyclerView recyclerView = findViewById(R.id.recyclerView);

        mAdapter = new AdapterData(itemList);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainMenuActivity.this);

        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(mAdapter);
        //recyler end here


        load = findViewById(R.id.LoadButton);
        load.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemList.clear();
                //mAdapter.addItem(new modelData("sunding","500000","sunding"));
                db.collection("menu").get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful()) {
                                    for (QueryDocumentSnapshot document : task.getResult()) {
                                        mAdapter.addItem(new modelData(document.getString("nama_menu"),
                                                document.getString("harga"),
                                                document.getString("deskripsi")));
                                        Log.d(TAG,document.getId() + " => " + document.getData());
                                    }
                                } else {
                                    Log.d(TAG, "Error getting documents: ", task.getException());
                                }
                            }
                        });
            }
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainMenuActivity.this, TambahMenu.class);
                startActivity(intent);
            }
        });
    }
    public void loadFirebase(){

    }
}

