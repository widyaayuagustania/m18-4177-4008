package com.example.dickson_1202160303_si4003_pab_modul4.Model;

public class modelData {
    private String nama_menu, harga_menu, des_menu;
    public modelData(String menu, String harga, String des){
        this.nama_menu = menu;
        this.harga_menu = harga;
        this.des_menu = des;
    }

    public String getNama_menu() {
        return nama_menu;
    }

    public void setNama_menu(String nama_menu) {
        this.nama_menu = nama_menu;
    }

    public String getHarga_menu() {
        return harga_menu;
    }

    public void setHarga_menu(String harga_menu) {
        this.harga_menu = harga_menu;
    }

    public String getDes_menu() {
        return des_menu;
    }

    public void setDes_menu(String des_menu) {
        this.des_menu = des_menu;
    }
}
