package com.example.widya_1202164177_si4008_pab_modul4.Activity;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.dickson_1202160303_si4003_pab_modul4.Model.modelData;
import com.example.dickson_1202160303_si4003_pab_modul4.R;

import java.util.ArrayList;

public class AdapterData extends RecyclerView.Adapter<AdapterData.viewHolder>{

    ArrayList<modelData> mlist = new ArrayList<modelData>();
    interface onclick{
        void onClick(modelData dataModel);
    }
    public AdapterData(ArrayList<modelData> mlist){
        this.mlist = mlist;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.row,viewGroup,false);
        return new viewHolder(view);
    }
    @Override
    public void onBindViewHolder(viewHolder viewholder,final int i){
        viewholder.txtNama.setText(mlist.get(i).getNama_menu());
        viewholder.txtHarga.setText(mlist.get(i).getHarga_menu());
    }
    public  void addItem(modelData data){
        mlist.add(data);
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount(){
        return (mlist != null) ? mlist.size() : 0;
    }
    public class viewHolder extends RecyclerView.ViewHolder{
        private TextView txtNama,txtHarga;
        public viewHolder(View itemView){
            super(itemView);
            txtNama = itemView.findViewById(R.id.namaItem);
            txtHarga = itemView.findViewById(R.id.hargaItem);
        }
    }
}
