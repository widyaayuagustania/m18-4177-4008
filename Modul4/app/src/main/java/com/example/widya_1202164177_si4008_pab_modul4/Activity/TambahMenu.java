package com.example.dickson_1202160303_si4003_pab_modul4.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.dickson_1202160303_si4003_pab_modul4.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;

import java.util.HashMap;
import java.util.Map;

public class TambahMenu extends AppCompatActivity {
private FirebaseStorage storage;
private FirebaseAuth auth;
private FirebaseFirestore db;
Button save,btnGambar;
TextView nama,harga,des;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_menu);
        storage = FirebaseStorage.getInstance();
        db = FirebaseFirestore.getInstance();
        save = findViewById(R.id.btnTambahMenu);
        auth = FirebaseAuth.getInstance();
        btnGambar = findViewById(R.id.btnGambar);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputMenu();
            }
        });
        btnGambar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                
            }
        });
    }
    public void inputMenu(){
        nama = findViewById(R.id.inputNamaMenu);
        harga = findViewById(R.id.inputHarga);
        des = findViewById(R.id.inputDeskripsi);
        Map<String,String> menuMap = new HashMap<>();
        menuMap.put("nama_menu",nama.getText().toString().trim());
        menuMap.put("harga",harga.getText().toString().trim());
        menuMap.put("deskripsi",des.getText().toString().trim());
        db.collection("menu").add(menuMap);
    }

    public void addDataRV(){

    }
}
